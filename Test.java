package com.peopleflow;

public class Test {

    public static void main(String[] args) {
        int[][] array1 = {
                {1, 4, -2},
                {3, 5, -6},
                {4, 5, 2}
        };

        int[][] array2 = {
                {5, 2, 8, -1},
                {3, 6, 4, 5},
                {-2, 9, 7, -3}
        };
        Test test = new Test();
        test.printArray(test.multiplication(array1, array2));
    }

    private int[][] multiplication(int[][] array1, int[][] array2) {
        int r1, r2, c1, c2;
        r1 = array1.length;
        c1 = array1[0].length;

        r2 = array2.length;
        c2 = array2[0].length;

        int[][] result;
        if (c1 != r2) {
            System.out.println("Error!");
            result = new int[0][0];
        } else {
            result = new int[r1][c2];

            for (int i = 0; i < r1; i++)//2
            {
                for (int j = 0; j < c2; j++)//4
                {
                    for (int k = 0; k < c1; k++) {
                        result[i][j] += array1[i][k] * array2[k][j];
                    }
                }
            }
        }

        return result;
    }

    private void printArray(int[][] array) {
        for (int[] arr : array) {
            for (int element : arr) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}
